#!/usr/bin/python2
'''
Author: Keyvan Hedayati <k1.hedayati93@gmail.com>
License: GNU General Public License, version 2
TODO: Improve code and OOP
'''
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from logging import debug, basicConfig, info, error, critical, DEBUG
from os.path import dirname, realpath, joinpath
from re import compile, IGNORECASE, DOTALL
from sqlite3 import connect, Error
from time import sleep
from urllib2 import urlopen, URLError

from twitter import Twitter, OAuth, TwitterHTTPError


class AutoTweet():

    def __init__(self, *args):
        '''
        Tries to create or connect to database and store instance in
        class variable db, if failed log error and exit.
        '''
        try:
            db_path = joinpath(dirname(realpath(__file__)), 'data.db')
            self.db = connect(db_path)
            self.twitter = self.get_twitter_object()
            self.tweet(*args)
        except Error as e:
            critical('Could not Connect to Database, ', e.args[0])
            exit(1)

    def get_twitter_object(self):
        '''
        Tries to get an instance of twitter object from oauth table data
        and return created Twitter instance, if faild log error and exit.
        '''
        try:
            cursor = self.db.cursor()
            result = cursor.execute('SELECT * FROM oauth').fetchone()
            auth = OAuth(result[0], result[1], result[2], result[3])
            return Twitter(auth=auth)
        except Error as e:
            critical('Could not Get OAuth Data, %s', e.args[0])
            exit(1)
        except TypeError as e:
            critical('First insert OAuth secrets in database, %s',
                     e.args[0])
            exit(1)

    def prepare_tweet(self, tweet_is_url, add_auto_tweet):
        '''
        Get a tweet from database then if tweet_is_url was true then
        gets title of URL and appends it tweet also if there is no
        internet connection waits until connection established, finally
        returns tweet string with #AutoTweet hash tag if add_auto_tweet
        was true, also catches some exception and handles then, such as
        database and URL errors
        '''
        try:
            cursor = self.db.cursor()
            id, tweet = cursor.execute('SELECT id, tweet FROM tweets WHERE \
                                        sent = 0 LIMIT 1').fetchone()
            self.id = id
            if tweet_is_url:
                if not (tweet.startswith('http://') or
                        tweet.startswith('https://')):
                    tweet = 'http://' + tweet
                self.wait_for_connection()
                page = urlopen(tweet).read()
                regex = compile('<title>(.*?)</title>',
                                IGNORECASE | DOTALL)

                title = regex.search(str(page)).group(1)
                tweet = title + ' ' + tweet
            return tweet + (' #AutoTweet' if add_auto_tweet else '')
        except URLError as e:
            critical('Could not Open URL, %s', e.args[0])
            exit(1)
        except TypeError as e:
            critical('First Insert Some Tweets into Database, %s',
                     e.args[0])
            exit(1)
        except Error as e:
            critical('Database Problem, Could not Get Tweet, %s',
                     e.args[0])
            exit(1)

    def mark_as_sent(self):
        '''
        After successfully tweeted a tweet updates its record to show
        that we are done with this tweet
        '''
        try:
            cursor = self.db.cursor()
            cursor.execute('UPDATE tweets SET sent = 1 WHERE id = ?',
                           (self.id,))
            self.db.commit()
        except Error as e:
            critical('Could not Mark As Sent, %s', e.args[0])

    def wait_for_connection(self, wait=60, retry=100):
        '''
        Waits untill conection establishes and can open twitter.com
        without error.
        '''
        for i in range(retry):
            try:
                response = urlopen('http://twitter.com', timeout=10)
                return
            except URLError:
                sleep(wait)

    def tweet(self, tweet_is_url, add_auto_tweet):
        '''
        Main method, tweets on twitter with waiting for internet connection,
        marking it as sent, logging and catching exceptions.
        '''
        try:
            tweet = self.prepare_tweet(tweet_is_url, add_auto_tweet).strip()
            info('Tweeting: %s', tweet)
            self.wait_for_connection()
            self.twitter.statuses.update(status=tweet)
            info('Tweeted Successfully')
            self.mark_as_sent()
        except URLError as e:
            critical('Connection refused, %s', e.args[0])
            exit(1)
        except TwitterHTTPError as e:
            critical('Authentication Problem, Check OAuth Values')
            exit(1)


class Install():
    def __init__(self, first_time=False):
        '''
        Connects to database, if first_time was true then installs database
        '''
        self.db = connect('data.db')
        if first_time:
            self.install_database()
            self.get_oauth()
            self.insert_tweets()

    def remove_table_data(self, tables):
        ''' Removes specified table from database. '''
        try:
            cursor = self.db.cursor()
            for table in tables:
                cursor.execute('DELETE FROM ' + table)
            self.db.commit()
        except Error as e:
            critical('Could not Delete%sData, %s', (' '.join(tables),
                                                    e.args[0]))
            exit(1)

    def install_database(self):
        ''' Create database tables  '''
        try:
            cursor = self.db.cursor()
            cursor.execute('CREATE TABLE IF NOT EXISTS tweets \
                            (id INTEGER PRIMARY KEY, tweet TEXT UNIQUE,\
                             sent INTEGER)')
            cursor.execute('CREATE TABLE IF NOT EXISTS oauth \
                            (access_token TEXT, access_token_secret TEXT, \
                             consumer_key TEXT, consumer_secret TEXT)')
            self.db.commit()
            critical('Database Created Successfully')
        except Error as e:
            self.db.rollback()
            critical('Database error occurred, %s', e.args[0])
            exit(1)

    def get_oauth(self):
        ''' Gets oauth data from user and stores them in database. '''
        try:
            consumer_key = raw_input('Enter Consumer Key: ')
            consumer_secret = raw_input('Enter Consumer Secret: ')
            access_token = raw_input('Enter Access Token: ')
            access_token_secret = raw_input('Enter Access Token Secret: ')

            cursor = self.db.cursor()
            self.remove_table_data(['oauth'])
            cursor.execute('INSERT INTO oauth VALUES (?, ?, ?, ?)',
                           (access_token, access_token_secret, consumer_key,
                           consumer_secret))
            self.db.commit()
            info('OAuth Data Inserted Successfully')
        except Error as e:
            self.db.rollback()
            critical('Could not insert oauth data, %s', e.args[0])
            exit(1)

    def insert_tweets(self):
        '''
        Gets tweets filename and inserts them into database, also removes
        trailing newline.
        '''
        try:
            cursor = self.db.cursor()
            filename = raw_input('Enter Tweets Filename: ')
            tweets = open(filename)
            for tweet in tweets:
                if tweet[-1:] == '\n':
                    tweet = tweet[:-1]
                cursor.execute('INSERT INTO tweets(tweet, sent) VALUES (?, ?)',
                               (tweet, 0))
            tweets.close()
            self.db.commit()
            info('Tweets Inserted Successfully')
        except IOError as e:
            critical('File not Found')
            exit(1)
        except Error as e:
            self.db.rollback()
            critical('Could not insert Tweets, %s', e.args[0])
            exit(1)


def main():
    '''
    Parses args, configs logging and call right function depending on given
    arguments
    '''
    args = parse_args()
    log_file = joinpath(dirname(realpath(__file__)), 'log')
    basicConfig(filename=log_file, level=DEBUG)
    if args.remove:
        Install().remove_table_data(args.remove)
    if args.install:
        if 'all' in args.install:
            Install(first_time=True)
        else:
            if 'db' in args.install:
                Install().install_database()
            if 'auth' in args.install:
                Install().get_oauth()
            if 'tweet' in args.install:
                Install().insert_tweets()
        exit(0)
    for i in range(args.num):
        AutoTweet(args.url, args.add_auto_tweet)
    exit(0)


def parse_args():
    ''' Parses arguments '''
    description = 'Automatically tweets from list'
    formatter_class = ArgumentDefaultsHelpFormatter
    parser = ArgumentParser(description=description,
                            formatter_class=formatter_class)
    parser.add_argument('-i',
                        '--install',
                        choices=('all', 'db', 'auth', 'tweet'),
                        help='Install database(db), oauth data(auth)\
                              and tweets to be tweeted(tweet)',
                        nargs='*')
    parser.add_argument('-r',
                        '--remove',
                        choices=('oauth', 'tweets'),
                        help='Removes inserted data from database',
                        nargs='*')
    parser.add_argument('-n',
                        '--num',
                        default=1,
                        help='Number of tweets to be tweeted at once',
                        type=int)
    parser.add_argument('-u',
                        '--url',
                        action='store_true',
                        default=True,
                        help='When tweet is a URL this option will try\
                              to get title of URL and append it to tweet'
                        )
    parser.add_argument('-a',
                        '--no-auto-tweet',
                        action='store_false',
                        dest='add_auto_tweet',
                        help='Whether to add #AutoTweet hash tag or not')
    return parser.parse_args()

if __name__ == '__main__':
    main()
