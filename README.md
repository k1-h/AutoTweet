###Description
Automatically tweets to Twitter on each run.

###Requirements
twitter module, install it with:

    pip install twitter
or download from:
https://pypi.python.org/pypi/twitter

###Specifications
+ Uses argparse, logging, re, sqlite3 and twitter modules

###Examples
First install database, oauth and tweets with "-i all" option:
    AutoTweet.py -i all
Then just run AutoTweet.py each time you want to send a tweet
    AutoTweet.py
Or Specify number of messages to be tweeted
    AutoTweet.py -n 4
You also can remove oauth or tweets data from database in order to install new one
    AutoTweet.py -r oauth

###How to use
    usage: AutoTweet.py [-h] [-i [{all,db,auth,tweet} [{all,db,auth,tweet} ...]]]
                        [-r [{oauth,tweets} [{oauth,tweets} ...]]] [-n NUM] [-u]
                        [-a]

    Automatically tweets from list

    optional arguments:
      -h, --help            show this help message and exit
      -i [{all,db,auth,tweet} [{all,db,auth,tweet} ...]], --install [{all,db,auth,tweet} [{all,db,auth,tweet} ...]]
                            Install database(db), oauth data(auth) and tweets to
                            be tweeted(tweet) (default: None)
      -r [{oauth,tweets} [{oauth,tweets} ...]], --remove [{oauth,tweets} [{oauth,tweets} ...]]
                            Removes inserted data from database (default: None)
      -n NUM, --num NUM     Number of tweets to be tweeted at once (default: 1)
      -u, --url             When tweet is a URL this option will try to get title
                            of URL and append it to tweet (default: True)
      -a, --no-auto-tweet   Whether to add #AutoTweet hash tag or not (default:
                            True)
